'use strict';

var tusk = tusk || {};

tusk.atom = (function(){
	
	var Atom = function(){};

	Atom.prototype.props = {
		module: "atom"
	};

	Atom.prototype.legacy = function(child){
		var key;
		for (key in child.props) {
			if (child.props.hasOwnProperty(key)) {
				this.props[key] = child.props[key];
			}
		}
		for (key in child) {
			if ((key !== "props") && (child.hasOwnProperty(key))) {
				this[key] = child[key];
			}
		}
		console.log("Atom legacy is called")
		return this;
	};




	Atom.prototype.legacy(tusk.event.prototype);

	return Atom;
})();