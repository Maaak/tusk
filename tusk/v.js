'use strict';

var tusk = tusk || {};

tusk.vid = 0;

tusk.view = (function(){
	
	var View = function(args){

		View.prototype = new tusk.atom();

		this.cid = "v"+tusk.vid++;							// custom ID
		this.props = {};									// propetrties of view
		this.model = null;									// model with data for rendering

		var key;
		for (key in args) {
			if (args.hasOwnProperty(key)) {
				this.props[key] = args[key];
			}
		}
	};





	

	return View;
})();