'use strict';

var tusk = tusk || {};

tusk.event = (function(){

	var EventControl = function(){};

	EventControl.prototype.props = {
		// pool for all events, that are rised
		ePool: {},
		// pool for all atoms, that are listening for events
		lPool: {},
	}

	EventControl.prototype.on = function(eventName, callback, context){
		this.props.ePool[eventName] = callback;
	};

	EventControl.prototype.trigger = function(eventName, args){
		this.props.ePool[eventName].apply(this, args);
	}

	return EventControl;
})();