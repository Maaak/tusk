'use strict';

var tusk = tusk || {};

(function(){
	

	var model = new tusk.model({
		test: "test1",
		start: true
	});
	var model2 = new tusk.model({
		test: "test2"
	});
	var model3 = model.legacy(new tusk.model({test: "test3"}));


	model3.on("changed", function(args){
		console.log("event changed");
	}, this);

	model3.trigger("changed", {prop: "propTEST"});
	
})();