'use strict';

var tusk = tusk || {};

tusk.mid = 0;

tusk.model = (function(){
	
	var Model = function(args){


		this.cid = "m"+tusk.mid++;							// custom ID
		//this.props = {};									// propetrties of model

		var key;
		for (key in args) {
			if (args.hasOwnProperty(key)) {
				this.props[key] = args[key];
			}
		}
	};

	


	Model.prototype = new tusk.atom();
	//Model.prototype.legacy(tusk.atom.prototype);

	return Model;
})();